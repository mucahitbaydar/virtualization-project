package com.baydar.virt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * Custom Conflict Exception class
 * 
 * 
 * @author Mücahit BAYDAR
 *
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException  {

	private static final long serialVersionUID = 7371749624676677539L;
	
	public ConflictException() {
		super();
	}

	public ConflictException(String message) {
		super(message);
	}

	public ConflictException(String message, Throwable cause) {
		super(message, cause);
	}
}
