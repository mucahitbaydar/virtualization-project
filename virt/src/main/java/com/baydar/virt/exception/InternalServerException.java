package com.baydar.virt.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
/**
 * Custom Internal Server Error Exception class
 * 
 * 
 * @author Mücahit BAYDAR
 *
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends RuntimeException   {

	private static final long serialVersionUID = -2998241843426650368L;
	
	public InternalServerException() {
		super();
	}

	public InternalServerException(String message) {
		super(message);
	}

	public InternalServerException(String message, Throwable cause) {
		super(message, cause);
	}

}
