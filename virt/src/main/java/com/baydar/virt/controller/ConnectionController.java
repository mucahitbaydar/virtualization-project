package com.baydar.virt.controller;

import java.util.UUID;

import org.libvirt.Connect;
import org.libvirt.LibvirtException;
/**
 * Connection Controller class
 * Includes methods which require libvirt connection
 * 
 * @author Mücahit BAYDAR
 *
 */
public class ConnectionController {

	/**
	 * Checks if there is a vm running with given domainName using libvirt connection
	 * @param domainName	virtual machine name
	 * @return	true if vm is actually running, false if not
	 */
	public boolean checkVMExists(String domainName) {
		Connect conn = null;
		try {
			conn = new Connect("qemu:///system", true);
		} catch (LibvirtException e) {
			System.out.println("exception caught:" + e);
			System.out.println(e.getError());
		}
		//Iterate through all vms and check if given domainname is one of them
		try {
			int[] ids = conn.listDomains();
			for (int i = 0; i < ids.length; i++) {
				if (conn.domainLookupByID(ids[i]).getName().equalsIgnoreCase(domainName)) {
					return true;
				}
			}
		} catch (LibvirtException e) {
			System.out.println("exception caught:" + e);
			System.out.println(e.getError());
		}
		return false;
	}

	/**
	 * Destroys vm with given domainName
	 * @param domainName	virtual machine name which is going to be destroyed
	 * @return	message about operation result. If OK process completed without error. 
	 * If any other string returns check for error message.
	 */
	public String killVM(String domainName) {
		Connect conn = null;
		try {
			conn = new Connect("qemu:///system", false);
		} catch (LibvirtException e) {
			System.out.println("exception caught:" + e);
			System.out.println(e.getError());
			return "Libvirt encountered an error. Process is halted." + e.getMessage();
		}
		//Iterate through vm list and try to destroy the one with given domainname
		try {
			int[] ids = conn.listDomains();
			for (int i = 0; i < ids.length; i++) {
				if (conn.domainLookupByID(ids[i]).getName().equalsIgnoreCase(domainName)) {
					conn.domainLookupByID(ids[i]).destroy();
				}
			}
		} catch (LibvirtException e) {
			System.out.println("exception caught:" + e);
			System.out.println(e.getError());
			return "Libvirt encountered an error. Process is halted." + e.getMessage();
		}
		//check if is destroyed
		if(checkVMExists(domainName)) {
			return "VM couldn't destroyed for an unkown reason. Request couldn't met.";
		}else {
			return "OK";
		}
		
	}

	/**
	 * Creates new vm with given vmname and image file
	 * @param vmName	Virtual machine name which is going to be created
	 * @param copyImagePath	image file which is going to be used for creating new vm
	 * @param MAC	MAC id for vm which is going to be created
	 * @return	message about operation result. If OK process completed without error. 
	 * If any other string returns check for error message.
	 */
	public String createNewVM(String vmName, String copyImagePath, String MAC) {
		Connect conn = null;
		try {
			conn = new Connect("qemu:///system", false);
		} catch (LibvirtException e) {
            System.out.println("exception caught:"+e);
            System.out.println(e.getError());
            return "Libvirt encountered an error. Process is halted." + e.getMessage();
		}
		UUID uuid = UUID.randomUUID();
		String vmUuid = uuid.toString();
		String imgFile = copyImagePath + vmName + ".qcow2";
		//xml file is going to used for creation process
		String xmlContent = "<domain type='kvm'>\n" + "  <name>" + vmName + "</name>\n" + "  <uuid>" + vmUuid
				+ "</uuid>\n" + "  <memory unit='KiB'>2097152</memory>\n"
				+ "  <currentMemory unit='KiB'>2097152</currentMemory>\n" + "  <vcpu placement='static'>1</vcpu>\n"
				+ "  <os>\n" + "    <type arch='x86_64'>hvm</type>\n" + "    <boot dev='hd'/>\n" + "  </os>\n"
				+ "  <features>\n" + "    <acpi/>\n" + "    <apic/>\n" + "  </features>\n" + "  <clock offset='utc'/>\n"
				+ "  <on_poweroff>destroy</on_poweroff>\n" + "  <on_reboot>restart</on_reboot>\n"
				+ "  <on_crash>restart</on_crash>\n" + "  <pm>\n" + "    <suspend-to-mem enabled='no'/>\n"
				+ "    <suspend-to-disk enabled='no'/>\n" + "  </pm>\n" + "  <devices>\n"
				+ "    <disk type='file' device='disk'>\n" + "      <driver name='qemu' type='qcow2'/>\n"
				+ "      <source file='" + imgFile + "'/>\n" + "      <target dev='vda' bus='virtio'/>\n"
				+ "    </disk>\n" + "    <interface type='bridge'>\n" + "      <mac address='" + MAC + "'/>\n"
				+ "      <source bridge='virbr0'/>\n" + "      <model type='virtio'/>\n" + "    </interface>\n"
				+ "    <input type='mouse' bus='ps2'/>\n" + "    <input type='keyboard' bus='ps2'/>\n"
				+ "    <graphics type='spice' port='5900' autoport='yes' listen='127.0.0.1'>\n"
				+ "      <listen type='address' address='127.0.0.1'/>\n" + "      <image compression='off'/>\n"
				+ "    </graphics>\n" + "    <sound model='ich6'>\n" + "      <alias name='sound0'/>\n"
				+ "    </sound>\n" + "    <video>\n"
				+ "      <model type='qxl' ram='65536' vram='65536' vgamem='16384' heads='1'/>\n"
				+ "      <alias name='video0'/>\n" + "    </video>\n" + "    <memballoon model='virtio'>\n"
				+ "      <alias name='balloon0'/>\n" + "    </memballoon>\n" + "  </devices>\n" + "</domain>";

		try {
			conn.domainCreateXML(xmlContent, 0);
		} catch (LibvirtException e) {
			e.printStackTrace();
			return "Libvirt encountered an error while creating VM. Process is halted." + e.getMessage();
		}
		//check if vm started successfully
		if(checkVMExists(vmName)) {
			return "OK";
		}else {
			return "Something went wrong and VM couldn't created. Your request couldn't met.";
		}
	}

}
