package com.baydar.virt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baydar.virt.exception.BadRequestException;
import com.baydar.virt.exception.ConflictException;
import com.baydar.virt.exception.InternalServerException;
import com.baydar.virt.model.VirtualMachine;
import com.baydar.virt.repository.VirtualMachineRepository;
import com.baydar.virt.utility.Util;
/**
 * Virtual Machine Controller class
 * Manages REST requests
 * 
 * @author Mücahit BAYDAR
 *
 */
@RestController
@RequestMapping("/api")
public class VirtualMachineController {

	static final ConnectionController connectionController = new ConnectionController();
	static final ProcessController processController = new ProcessController();
	
	/**
	 * Reads base image path from application.properties
	 */
	@Value("${virt.baseImagePath}")
	private String baseImagePath;
	/**
	 * Reads copy image path from application.properties
	 */
	@Value("${virt.copyImagePath}")
	private String copyImagePath;

	/**
	 * Returns base image path
	 */
	public String retrieveBaseImagePath() {
		return baseImagePath;
	}
	/**
	 * Returns copy image path
	 */
	public String retrieveCopyImagePath() {
		return copyImagePath;
	}
	
	@Autowired
	VirtualMachineRepository vmRepository;

	/**
	 * Manages /api/vm REST Get request
	 * @return all virtual machines from repository
	 */
	@GetMapping("/vm")
	public List<VirtualMachine> getAllVMss() {
		//if vm is destroyed by external process we need to delete it from our repo as well
		List<VirtualMachine> vmList = vmRepository.findAll(); 
		if(vmList!=null) {
			for(int i = 0; i < vmList.size(); i++) {
				if(!connectionController.checkVMExists(vmList.get(i).getName())) {
					vmRepository.delete(vmList.get(i));
				}
			}
		}
		return vmRepository.findAll();
	}

	/**
	 * Manages /api/vm/start/{MAC} Post request
	 * @param MAC	MAC id of virtual machine which is trying to be created
	 * @return	ResponseEntity with created vm or if there is an error returns exception
	 */
	@PostMapping("/vm/start/{MAC}")
	public ResponseEntity<VirtualMachine> createVM(@PathVariable(value = "MAC") String MAC) {
		//check if given MAC is valid
		if(!Util.isValidMacAddress(MAC)) {
			throw new BadRequestException("Given MAC in the URL is not a valid one. Please make a request with valid MAC.");
		}
		//check if properties are set
		if(retrieveBaseImagePath().equals("") || retrieveCopyImagePath().equals("")) {
			throw new BadRequestException("Properties virt.baseImagePath and virt.copyImagePath should be set.");
		}
		//check if base image file exists
		if(!Util.checkFile(retrieveBaseImagePath())) {
			throw new BadRequestException("Base image file couldn't found. Property virt.baseImagePath should be set correctly.");
		}
		
		String vmName = Util.getVMName(MAC);
		if (vmRepository.getVMByMAC(MAC)!=null || connectionController.checkVMExists(vmName)) {
			throw new ConflictException("There is already a VM running with this MAC : " + MAC + "\n. Please try to make request with another MAC.");
		}
		//check result returning from createing new image file method
		String result = processController.createNewImageFile(retrieveBaseImagePath(), retrieveCopyImagePath(), vmName);
		if(!result.equals("OK")) {
			throw new ConflictException(result);
		}
		//check result returning from creating new vm method
		result = connectionController.createNewVM(vmName, retrieveCopyImagePath(), MAC);
		if(!result.equals("OK")){
			throw new InternalServerException(result);
		}
		//if everything works perfectly then create new vm object and save it
		VirtualMachine virtualMachine = new VirtualMachine();
		virtualMachine.setMAC(MAC);
		virtualMachine.setName(vmName);
		virtualMachine.setStatus(0); // it is created. until it is booted status set to 0(booting)
		vmRepository.save(virtualMachine);
		return new ResponseEntity<>(virtualMachine, null,HttpStatus.CREATED);
	}
	
	/**
	 * Manages /api/vm/stop/{MAC} Delete request
	 * @param MAC	MAC id of virtual machine which is trying to be destroyed
	 * @return	ResponseEntity with message or if there is an error returns exception
	 */
	@DeleteMapping("/vm/stop/{MAC}")
	public ResponseEntity<String> destroyVM(@PathVariable(value = "MAC") String MAC) {
		//check if given MAC is valid
		if(!Util.isValidMacAddress(MAC)) {
			throw new BadRequestException("Given MAC in the URL is not a valid one. Please make a request with valid MAC.");
		}
		//check if property is set
		if(retrieveCopyImagePath().equals("")) {
			throw new BadRequestException("Property virt.copyImagePath should be set.");
		}
		//check if vm exists with given MAC
		VirtualMachine vm = vmRepository.getVMByMAC(MAC);
		if (vm == null) {
			throw new ResourceNotFoundException("VM with this MAC : " + MAC + " couldn't found. Your request couldn't met.");
		}
		String vmName = vm.getName();
		//check result returning from destroying vm method
		String result = connectionController.killVM(vmName);
		if(!result.equals("OK")) {
			throw new InternalServerException(result);
		}
		vmRepository.delete(vm);
		//check if vm related files deleted succesfully
		if(Util.deleteFile(vmName,retrieveCopyImagePath())) {
			return new ResponseEntity<String>("VM Destroyed Successfully.",null,HttpStatus.OK);
		}else {
			return new ResponseEntity<String>("VM Destroyed Successfully. But image file of the VM couldn't deleted.", null,HttpStatus.OK);
		}		
	}
	
	/**
	 * Manages /api/vm/status/{MAC} Get request
	 * @param MAC	MAC id of virtual machine whose status will return
	 * @return	ResponseEntity with status or if there is an error returns exception
	 */
	@GetMapping("/vm/status/{MAC}")
	public ResponseEntity<String> VMStatus(@PathVariable(value = "MAC") String MAC) {
		//check if given MAC is valid
		if(!Util.isValidMacAddress(MAC)) {
			throw new BadRequestException("Given MAC in the URL is not a valid one. Please make a request with valid MAC.");
		}
		//check if vm exists with given MAC
		if(!connectionController.checkVMExists(Util.getVMName(MAC))) {
			//vm could be destroyed in an unexpected way
			//delete object from repository
			VirtualMachine vm = vmRepository.getVMByMAC(MAC);
			if(vm != null) {
				vmRepository.delete(vm);
			}
			return new ResponseEntity<String>("Stopped",null,HttpStatus.OK);
		}else {
			VirtualMachine vm = vmRepository.getVMByMAC(MAC);
			//if vm couldn't find in respository return stopped
			if (vm == null) {
				return new ResponseEntity<String>("Stopped",null,HttpStatus.OK);
			}
			int status = vm.getStatus();
			if(status == 0) {
				return new ResponseEntity<String>("Booting",null,HttpStatus.OK);
			}else if(status == 1) {
				return new ResponseEntity<String>("Started",null,HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("Houston, we have a problem here.",null,HttpStatus.OK);
			}
		}
	}

	/**
	 * Manages /api/vm/ack/{MAC} Put request
	 * @param MAC	MAC id of virtual machine whose status will change
	 * @return	ResponseEntity with requested vm or if there is an error returns exception
	 */
	@PutMapping("/vm/ack/{MAC}")
	public ResponseEntity<VirtualMachine> getVMAck(@PathVariable(value = "MAC") String MAC) {
		//check if given MAC is valid
		if(!Util.isValidMacAddress(MAC)) {
			throw new BadRequestException("Given MAC in the URL is not a valid one. Please make a request with valid MAC.");
		}
		//check if vm exists with given MAC
		VirtualMachine vm = vmRepository.getVMByMAC(MAC);
		if (vm == null) {
			throw new ResourceNotFoundException("VM with this MAC : " + MAC + " couldn't found. Your request couldn't met.");
		}
		//if vm exists in repository and not running then it could be killed by some other processes
		//we need to delete it from repository and return an error message
		if(!connectionController.checkVMExists(Util.getVMName(MAC))) {
			vmRepository.delete(vm);
			throw new ResourceNotFoundException("VM with this MAC : " + MAC + " couldn't found. Your request couldn't met.");
		}
		vm.setStatus(1); // set status to started
		vmRepository.save(vm);
		return new ResponseEntity<VirtualMachine>(vm,null,HttpStatus.OK);
	}

}
