package com.baydar.virt.controller;

import java.io.IOException;

import com.baydar.virt.utility.Util;
/**
 * Process Controller Class
 * Includes methods for process usage
 * 
 * @author Mücahit BAYDAR
 *
 */
public class ProcessController {

	/**
	 * Creates new image file using process and qemu-image command
	 * @param baseImagePath	original image file
	 * @param copyImagePath	file path where copy image is going to put
	 * @param vmName	virtual machine name is going to be used as file name
	 * @return	message about process result. If OK process completed without error. 
	 * If any other string returns check for error message.
	 */
	public String createNewImageFile(String baseImagePath, String copyImagePath, String vmName) {
		// check if file exists
		if (Util.checkFile(copyImagePath + vmName + ".qcow2")) {
			// try to delete it first
			if (!Util.deleteFile(vmName, copyImagePath)) {
				return "The file in path " + copyImagePath + " with this name " + vmName
						+ ".qcow2 already exists. \n We tried to delete it first but couldn't achieve it. The process is terminating now.";
			}
		}

		// first create copy of image file
		String[] args = new String[] { "/bin/bash", "-c",
				"qemu-img create -f qcow2 -b " + baseImagePath + " " + copyImagePath + vmName + ".qcow2" };
		try {
			Process proc = new ProcessBuilder(args).start();
		} catch (IOException e) {
			e.printStackTrace();
			return "We have encountered an error while creating copy of image file. The process is halting now. \n " + e.getMessage();
		}
		//wait a little for copying process 
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//check if new file exists
		if(!Util.checkFile(copyImagePath + vmName + ".qcow2")) {
			return "Copy of image file couldn't created. The process is terminating now.";
		}
		return "OK";
	}

}
