package com.baydar.virt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.baydar.virt.model.VirtualMachine;
/**
 * Repository interface extends JpaRepository
 * 
 * @author Mücahit BAYDAR
 *
 */
public interface VirtualMachineRepository extends JpaRepository<VirtualMachine, Long>{

	/**
	 * default method returns virtual machine with given MAC
	 * @param MAC	MAC id of virtual machine
	 * @return	VirtualMachine if exists, null if not 
	 */
	default VirtualMachine getVMByMAC(String MAC) {
		List<VirtualMachine> vmList = this.findAll();
		for(int i = 0;i < vmList.size() ; i++) {
			if(vmList.get(i).getMAC().equalsIgnoreCase(MAC)) {
				return vmList.get(i);
			}
		}
		return null;
	}
	
}
