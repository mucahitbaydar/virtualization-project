package com.baydar.virt.utility;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;
/**
 * Util class
 * Consists of useful methods such as 
 * checking if file exists and checking if given mac is valid
 * 
 * @author Mücahit BAYDAR
 *
 */
public final class Util {

	/**
	 * Pattern for valid mac pairs
	 */
	private static Pattern patternMacPairs = Pattern.compile("^([a-fA-F0-9]{2}[:]?){5}[a-fA-F0-9]{2}$");
	
	/**
	 * Checks if the file exists with given string
	 * @param filePathString absoloute file path string
	 * @return true if file exists, false if file doesn't exist
	 */
	public static boolean checkFile(String filePathString) {
		Path path = Paths.get(filePathString);
		if (Files.exists(path)) {
		  return true;
		}else {
			return false;
		}
	}
	
	/**
	 * Deletes file with concatenating given two parameters and adding file type at the end 
	 * @param vmName	virtual machine name string
	 * @param copyImagePath	file path as a string
	 * @return	true if given file deleted successfully, false if file still exists
	 */
	public static boolean deleteFile(String vmName, String copyImagePath) {
		Path path = Paths.get(copyImagePath+vmName+".qcow2");
		try {
		    Files.delete(path);
		} catch (NoSuchFileException x) {
		    System.err.format("%s: no such" + " file or directory%n", path);
		} catch (DirectoryNotEmptyException x) {
		    System.err.format("%s not empty%n", path);
		} catch (IOException x) {
		    System.err.println(x);
		}
		if(Files.exists(path)) {
			return false;
		}else {
			return true;
		}
	}
	/**
	 * 
	 * @param mac	MAC id of virtual machine
	 * @return		remove all semicolons and dots and return given mac
	 */
	public static String getVMName(String mac) {
		return mac.replace(":", "").replaceAll("\\.", "");
	}
	
	/**
	 * 
	 * @param mac 	MAC id of virtual machine
	 * @return		true if given MAC is valid, false if invalid
	 */
	public static boolean isValidMacAddress(String mac)
	{
	    return (patternMacPairs.matcher(mac).find());
	}
	

}
