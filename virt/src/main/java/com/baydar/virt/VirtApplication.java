package com.baydar.virt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Main Class of Spring Application
 * 
 * @author Mücahit BAYDAR
 *
 */
@SpringBootApplication
public class VirtApplication {

	public static void main(String[] args) {
		SpringApplication.run(VirtApplication.class, args);
	}
}
