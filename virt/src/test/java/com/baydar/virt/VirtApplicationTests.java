package com.baydar.virt;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
/**
 * Test class for testing rest requests
 * 
 * @author Mücahit BAYDAR
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class VirtApplicationTests {

	/**
	 * Mockmvc object is used for rest testing
	 */
	@Autowired
	private MockMvc mockMvc;

	/**
	 * Test 
	 * This method creates 1 vm then checks status of created vm.
	 * Then sends ack to change vm status to started and then checks it.
	 * Then stops the vm and checks the status again which should be stopped atm.
	 * @throws Exception
	 */
	@Test
	public void shouldCreateStatusAcknowledgeStatusDelete() throws Exception {
		this.mockMvc.perform(post("/api/vm/start/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isCreated());
		this.mockMvc.perform(get("/api/vm/status/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Booting"));
		this.mockMvc.perform(put("/api/vm/ack/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk());
		this.mockMvc.perform(get("/api/vm/status/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Started"));
		this.mockMvc.perform(delete("/api/vm/stop/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("VM Destroyed Successfully."));
		this.mockMvc.perform(get("/api/vm/status/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Stopped"));
		this.mockMvc.perform(get("/api/vm")).andDo(print()).andExpect(status().isOk()).andExpect(content().json("[]"));
	}

	/**
	 * Test 
	 * This method first creates 1 vm and then checks its status.
	 * Then sends ack to change vm status to started and then checks it.
	 * Follows the same procedure for the second vm.
	 * Then retrieves vm lists and check their MACs.
	 * At last stops both vm and check their status and see if they are both stopped.
	 * @throws Exception
	 */
	@Test
	public void shouldCreateMultipleVMThenDelete() throws Exception {
		this.mockMvc.perform(post("/api/vm/start/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isCreated());
		this.mockMvc.perform(get("/api/vm/status/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Booting"));
		this.mockMvc.perform(put("/api/vm/ack/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk());
		this.mockMvc.perform(get("/api/vm/status/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Started"));
		this.mockMvc.perform(post("/api/vm/start/3A:57:96:B0:67:1D")).andDo(print()).andExpect(status().isCreated());
		this.mockMvc.perform(get("/api/vm/status/3A:57:96:B0:67:1D")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Booting"));
		this.mockMvc.perform(put("/api/vm/ack/3A:57:96:B0:67:1D")).andDo(print()).andExpect(status().isOk());
		this.mockMvc.perform(get("/api/vm/status/3A:57:96:B0:67:1D")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Started"));
		this.mockMvc.perform(get("/api/vm")).andDo(print()).andExpect(jsonPath("$[0].mac", is("AE:A4:28:AB:BB:55")));
		this.mockMvc.perform(get("/api/vm")).andDo(print()).andExpect(jsonPath("$[1].mac", is("3A:57:96:B0:67:1D")));
		this.mockMvc.perform(delete("/api/vm/stop/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("VM Destroyed Successfully."));
		this.mockMvc.perform(get("/api/vm/status/AE:A4:28:AB:BB:55")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Stopped"));
		this.mockMvc.perform(delete("/api/vm/stop/3A:57:96:B0:67:1D")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("VM Destroyed Successfully."));
		this.mockMvc.perform(get("/api/vm/status/3A:57:96:B0:67:1D")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Stopped"));
		this.mockMvc.perform(get("/api/vm")).andDo(print()).andExpect(status().isOk()).andExpect(content().json("[]"));
	}

	
	/**
	 * Test
	 * This method tries to start a vm with invalid MAC 
	 * and check if gets bad request error.
	 * @throws Exception
	 */
	@Test
	public void shouldReturnErrorInvalidMAC1() throws Exception {
		this.mockMvc.perform(post("/api/vm/start/1234")).andDo(print()).andExpect(status().isBadRequest());
	}

	/**
	 * Test
	 * This method tries to stop a vm with invalid MAC 
	 * and check if gets bad request error.
	 * @throws Exception
	 */
	@Test
	public void shouldReturnErrorInvalidMAC2() throws Exception {
		this.mockMvc.perform(delete("/api/vm/stop/123456")).andDo(print()).andExpect(status().isBadRequest());
	}

	/**
	 * Test
	 * This method tries to retrieve status of a vm with invalid MAC 
	 * and check if gets bad request error.
	 * @throws Exception
	 */
	@Test
	public void shouldReturnErrorInvalidMAC3() throws Exception {
		this.mockMvc.perform(get("/api/vm/status/123456789")).andDo(print()).andExpect(status().isBadRequest());
	}

	/**
	 * Test
	 * This method tries to send ack request to a vm with invalid MAC 
	 * and checks if gets bad request error.
	 * @throws Exception
	 */
	@Test
	public void shouldReturnErrorInvalidMAC4() throws Exception {
		this.mockMvc.perform(put("/api/vm/ack/12345678910")).andDo(print()).andExpect(status().isBadRequest());
	}

	/**
	 * Test
	 * This method tries to create two vms with same MAC and 
	 * checks if gets 
	 * and check if gets bad request error.
	 * @throws Exception
	 */
	@Test
	public void shouldReturnConflictWhenTryingToCreate2VMsWithSameMAC() throws Exception {
		this.mockMvc.perform(post("/api/vm/start/76:b9:49:23:3e:93")).andDo(print()).andExpect(status().isCreated());
		this.mockMvc.perform(post("/api/vm/start/76:b9:49:23:3e:93")).andDo(print()).andExpect(status().isConflict());
		this.mockMvc.perform(delete("/api/vm/stop/76:b9:49:23:3e:93")).andDo(print()).andExpect(status().isOk());
	}

	/**
	 * Test
	 * This method tries to stop a vm which doesn't exist
	 * and gets not found error
	 * @throws Exception
	 */
	@Test
	public void shouldReturnNotFound1() throws Exception {
		this.mockMvc.perform(delete("/api/vm/stop/c4:8d:5e:d3:dc:7f")).andDo(print()).andExpect(status().isNotFound());
	}

	/**
	 * Test
	 * This method tries to ack a vm which doesn't exist
	 * and gets not found error
	 * @throws Exception
	 */
	@Test
	public void shouldReturnNotFound2() throws Exception {
		this.mockMvc.perform(put("/api/vm/ack/c4:8d:5e:d3:dc:7f")).andDo(print()).andExpect(status().isNotFound());
	}

	/**
	 * Test
	 * This method tries to stop a vm which doesn't exist
	 * and gets Stopped status
	 * @throws Exception
	 */
	@Test
	public void shouldReturnStopped() throws Exception {
		this.mockMvc.perform(get("/api/vm/status/c4:8d:5e:d3:dc:7f")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string("Stopped"));
	}

	/**
	 * Test
	 * This method tries to send rest request which
	 * doesn't cover by the api and gets not found error
	 * @throws Exception
	 */
	@Test
	public void shouldReturnNotFound() throws Exception {
		this.mockMvc.perform(get("/api/vm/badrequest")).andDo(print()).andExpect(status().isNotFound());
	}
	
	/**
	 * Test
	 * This method tries to retrieve all running vms 
	 * and gets an emtpy list
	 * @throws Exception
	 */
	@Test
	public void shouldReturnEmptyList() throws Exception {
		this.mockMvc.perform(get("/api/vm")).andDo(print()).andExpect(status().isOk()).andExpect(content().json("[]"));
	}

}
