#Virtualization Project
This project is developed for part of a job hiring process.

##Prerequisites
There are several requirements to install and run the project correctly.

-Project needs to be used on Linux platform.

-Linux system must be running on hardware directly, not in a virtual machine.

-A qcow2 image file must be present in Linux system. 

-git should be installed in order to download project

-maven should be installed in order to build project

-jdk should be installed in order to run maven

You can download the project with following command
```
git clone https://mucahitbaydar@bitbucket.org/mucahitbaydar/virtualization-project.git
```

-libvirt and qemu-kvm libraries should be installed. Application uses libvirt java bindings.

For installing libvirt one can uses following command
```
sudo apt install qemu-kvm libvirt-bin
```
-libvirt java binding file should be added to maven repository through following command
```
mvn install:install-file -Dfile=<path-to-java-binding-jar> -DgroupId=org.libvirt -DartifactId=libvirt -Dversion=0.5.0 -Dpackaging=jar
```
Libvirt java binding jar(libvirt-0.5.0.jar) exists in project's root folder and <path-to-java-binding-jar> should be changed to show this file.

application.properties file contains two properties

-virt.baseImagePath should set where the image file is located(qcow2)

-virt.copyImagePath should set where new files should be put

At this point we are ready to build project. At project root folder following command will build application.
```
mvn install
```
There is one final step to take. You need to create a script file inside the original virtual machine that executes at starts up. This script tells our application that virtual machine is booted up successfully.
To achieve this you need to add following code into this script file. In order to run following code curl must be installed.
```
MAC=$(cat /sys/class/net/ens3/address)
echo "MAC: $MAC"
IP=$(ip -4 r show default | awk '{print $3}')
echo "IP: $IP"
while test -z "$IP";do
sleep 1
IP=$(ip -4 r show default | awk '{print $3}')
done
curl -X PUT $IP:8080/api/vm/ack/$MAC
```
Above code waits until it reachs required IP then sends a signal that virtual machine is booted successfully.
##Usage
After build you can start the application with following command
```
java -jar virt-0.0.1-SNAPSHOT.jar
```

API Usage

There are 3 REST requests that users can try

-To create a new virtual machine and start it send post request with following url
```
localhost:8080/api/vm/start/<MAC> 
```
-To check running virtual machine's status send get request with following url
```
localhost:8080/api/vm/status/<MAC> 
```
-To destroy running virtual machine and delete all its resources send delete request with following url
```
localhost:8080/api/vm/stop/<MAC> 
```
MAC id should be a valid one. Only 6 duals with semicolons as seperator is allowed.

##Running the test
Test are ran by maven while building the project.

To run only the test 
```
mvn test
```
can be used.